# Fire Effect Demo Project #

### Summary ###

This project consists of the implementation of a very simple "fire-effect" which is described [here](https://www.youtube.com/watch?v=_SzpMBOp1mE).
The purpose of this assignment is to demonstrate coding skills in modern C++, provide good code in terms of readability and structure.

The task was to implement a demo application using C++/QT/Cmake.

### Functionality ###

![](demo.gif)

* The application opens a window that shows the fire
* Color of the fire is controlled by the left slider
* Speed of fire decay is controlled by the right slider
* Fire shape (intensity of fire-effect seeds) is controlled by the left checkbox. 
The application provides 2 modes for fire shape: uniform and bell-like.
* Fireplace fogging is simulated with simple blurring from OpenCv and can be controlled by the right checkbox.

### Structure of code ###

As a template for QT-OpenGl application I used [this project structure](https://doc.qt.io/qt-5/qtopengl-2dpainting-example.html) and this [control examples](https://doc.qt.io/qt-5/qtopengl-hellogl2-example.html). So, *window* and *glwidget* look like corresponding classes from these examples and provide QT windows functionality.

*FireSimulator* is the main class responsible for providing colors for image data. The color palette has a constant size and its minimum value is defined by the slider.

 The "fire-effect" itself is achieved by a simple formula. Every new pixel intensity is calculated as an average of 3 adjacent pixels from the previous line and subtracted by the decay component. The shape of the fire is defined by intensities of the first row. Classes *FlameGeneratorBase* and *FlameGeneratorBell* are responsible for 2 different flame generators. Other effects such a wind etc. can be also implemented in a similar manner.

 *ImagePostProcessor* is a helper with a simple OpenCv blur method. Kernel size for blurring is changing every frame, firstly increase and then decrease.

### How do I get set up? ###

Application has 2 major dependencies: QT and OpenCv.

[How to setup Qt and openCV on Windows](https://wiki.qt.io/How_to_setup_Qt_and_openCV_on_Windows)

[OpenCv Linux installation](https://docs.opencv.org/2.4/doc/tutorials/introduction/linux_install/linux_install.html?highlight=installation)

2 environment variables should be defined:

1. ${OpenCV_INCLUDE_DIRS}. For example, "c:/Libs/opencv/build/include".

2. ${OpenCV_LIBS}. For example, "c:/Libs/opencv/build/x64/vc15/lib/opencv_world440d.lib"

Project files for Windows (Visual Studio) and Linux (Cmake) are attached. QtCreator .pro file is also available.