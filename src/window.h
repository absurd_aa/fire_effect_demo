#pragma once

#include <memory>

#include <QSlider>
#include <QCheckBox>


namespace FireEffectDemo
{
    class Window : public QWidget
    {
        Q_OBJECT

    public:
        Window();

    private:
        [[nodiscard]] std::unique_ptr<QSlider> createSlider(int startPosition, int start = 0, int last = UCHAR_MAX,
            int step = 10, int tick = 55) const;

        std::unique_ptr<QSlider> _hueSlider;
        std::unique_ptr<QSlider> _decaySlider;
        std::unique_ptr<QCheckBox> _blurCheckBox;
        std::unique_ptr<QCheckBox> _flameTypeCheckBox;
    };
}
