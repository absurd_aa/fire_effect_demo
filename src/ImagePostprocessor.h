#pragma once

class QImage;
namespace FireEffectDemo
{
    class ImagePostprocessor
    {
    public:
        QImage doBlur(QImage& im);
        void reset();

    private:
        //we simulate fireplace fogging with the slow growth of blur and fast decay
        int calcKernelSize();

        const double _startSize = 1;
        const double _maxSize = 60;
        const double _upStep = 0.3;
        const double _downStep = -2;
        double _curStep = 0;
        int _direction = 1;
    };
}

