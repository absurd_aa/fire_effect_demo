#include "FireSimulator.h"

#include <QPainter>
#include "FlameGeneratorBell.h"
#include "Utils.h"
using namespace FireEffectDemo;

FireSimulator::FireSimulator(const int width, const int height): _width(width), _height(height), _minNonNullLineId(static_cast<size_t>(height) - 1)
{
    _intensityTable = std::vector<unsigned char>(static_cast<size_t>(width) * height);
    _colorPalette = std::vector<QColor>(_paletteSize);
    //by default hue goes from red to yellow
    _minHue = 0;
    _minPossibleHue = _paletteSize - _paletteSize / _huePaletteSizeRatio;
    setBaseFlame();
    fillPalette();
}

unsigned FireSimulator::getColor(const int i, const int j) const
{
    return _colorPalette[_intensityTable[at(i, j)]].rgb();
}

void FireSimulator::setHue(const unsigned char minValue)
{
    _minHue = std::min(minValue, _minPossibleHue);
    fillPalette();
}

void FireSimulator::setDecay(const unsigned char decayValue)
{
    _flameGenerator->setDecay(decayValue);
}

void FireSimulator::update()
{
    //randomize the bottom row of the fire buffer
    for (size_t x = 0; x < _width; x++)
    {
        _intensityTable[at(x, _height - 1)] = _flameGenerator->calcInitialIntensity(x);
    }

    _minNonNullLineId = _height - 1;
    //calc intensity for every pixel
    for (auto y = _height - 1; y > 0; y--)
    {
        auto sum = 0;
        for (size_t x = 0; x < _width; x++)
        {
            const auto i = _flameGenerator->calcIntensity(_intensityTable, x, y - 1);
            _intensityTable[at(x, y - 1)] = i;
            sum += i;
        }
        if (sum == 0)
            break;
        _minNonNullLineId = y;
    }
}

void FireSimulator::setBaseFlame()
{
    _flameGenerator = std::make_unique<FlameGeneratorBase>(0.5, 10, 2, _paletteSize, _width, _height);
}

void FireSimulator::setBellFlame()
{
    _flameGenerator = std::make_unique<FlameGeneratorBell>(1.5, 10, 1.5, _paletteSize, _width, _height);
}

int FireSimulator::width() const
{
    return static_cast<int>(_width);
}

int FireSimulator::height() const
{
    return static_cast<int>(_height);
}

size_t FireSimulator::minNonNullLine() const
{
    return _minNonNullLineId;
}

size_t FireSimulator::at(const size_t x, const size_t y) const
{
    return Utils::at(x, y, _width, _height);
}

void FireSimulator::fillPalette()
{
    //generate the palette
    for (auto intensity = 0; intensity < _paletteSize; intensity++)
    {
        //HSL to RGB
        //color is managed by hue and lightness, it allows us make gradient for fir simulation
        _colorPalette[intensity] = QColor::fromHsl(calcHue(intensity), _paletteSize - 1, calcLightness(intensity));
    }
}

unsigned char FireSimulator::calcHue(const int id) const
{
    return _minHue + id / _huePaletteSizeRatio;
}

unsigned char FireSimulator::calcLightness(const int id) const
{
    return std::min(_paletteSize - 1, id * 2);
}
