#pragma once
#include <vector>
#include <random>
#include <memory>

#include "FlameGeneratorBase.h"

class QColor;
namespace FireEffectDemo
{
    class FireSimulator
    {
    public:
        FireSimulator(int width, int height);
        ~FireSimulator(void) = default;
        FireSimulator(const FireSimulator&) = default;
        FireSimulator& operator= (const FireSimulator&) = default;

        [[nodiscard]] unsigned int getColor(int i, int j) const;
        void setHue(unsigned char minValue);
        void setDecay(unsigned char decayValue);
        void update();

        void setBaseFlame();
        void setBellFlame();

        [[nodiscard]] int width() const;
        [[nodiscard]] int height() const;
        [[nodiscard]] size_t minNonNullLine() const;

    private:
        [[nodiscard]] size_t at(size_t x, size_t y) const;
        void fillPalette();
        [[nodiscard]] unsigned char calcHue(int id) const;
        [[nodiscard]] unsigned char calcLightness(int id) const;

        //parameters
        size_t _width;
        size_t _height;
        int _paletteSize = UCHAR_MAX + 1;
        int _huePaletteSizeRatio = 3;
        unsigned char _minPossibleHue;

        //dynamic parameters
        unsigned char _minHue;

        //flame generator
        std::unique_ptr<FlameGeneratorBase> _flameGenerator;

        //state
        std::vector<unsigned char> _intensityTable;
        std::vector<QColor> _colorPalette;
        //little optimization trick for intensity table recalculation
        //this part can be optimized more
        size_t _minNonNullLineId;
    };
}

