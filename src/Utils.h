#pragma once

#include <qimage.h>
#include <opencv2/core/mat.hpp>

namespace FireEffectDemo
{
    class Utils
    {
    public:
        static size_t at(size_t x, size_t y, size_t width, size_t height);

        static cv::Mat qImageToMat(QImage& img, int format = CV_8UC4);

        static QImage matToQImage(const cv::Mat& mat, QImage::Format format = QImage::Format_RGB32);
    };
}

