#pragma once
#include <vector>
#include <random>

namespace FireEffectDemo
{
    class FlameGeneratorBase
    {
    public:
        FlameGeneratorBase(double minDecay, double maxDecay, double initDecay, int paletteSize, size_t width, size_t height);
        virtual ~FlameGeneratorBase() = default;
        FlameGeneratorBase(const FlameGeneratorBase&) = default;
        FlameGeneratorBase& operator= (const FlameGeneratorBase&) = default;
        //we use the same principe for both flame generators, but this method can be abstract
        [[nodiscard]] virtual unsigned char calcIntensity(const std::vector<unsigned char>& intensityTable, size_t x, size_t y) const;
        [[nodiscard]] virtual unsigned char calcInitialIntensity(size_t);
        //now we use constant decay, but it also can depend on position
        [[nodiscard]] virtual double calcDecay(size_t, size_t) const;
        void setDecay(unsigned char decayValue);

    protected:
        int _paletteSize;
        size_t _width;

    private:

        [[nodiscard]] size_t at(size_t x, size_t y) const;

        double _minDecay;
        double _maxDecay;
        double _decay;

        size_t _height;
        //random number generator
        std::mt19937 _gen;
        std::uniform_int_distribution<> _dist;
    };
}

