#include "window.h"

#include <QGridLayout>
#include <QTimer>

#include "glwidget.h"

using namespace FireEffectDemo;

Window::Window()
{
    setWindowTitle(tr("Fire Effect Demo"));

    const auto glWidget = new GLWidget(std::make_unique<FireSimulator>(720, 480), this);

    _hueSlider = createSlider(0);
    _decaySlider = createSlider(45);
    _blurCheckBox = std::make_unique<QCheckBox>();
    _flameTypeCheckBox = std::make_unique<QCheckBox>();
    connect(_hueSlider.get(), &QSlider::valueChanged, glWidget, &GLWidget::setHue);
    connect(_decaySlider.get(), &QSlider::valueChanged, glWidget, &GLWidget::setDecay);
    connect(_flameTypeCheckBox.get(), &QCheckBox::toggled, glWidget, &GLWidget::setFlame);
    connect(_blurCheckBox.get(), &QCheckBox::toggled, glWidget, &GLWidget::setBlur);

    auto layout = new QGridLayout;
    layout->addWidget(glWidget, 0, 0);
    layout->addWidget(_hueSlider.get(), 0, 1);
    layout->addWidget(_decaySlider.get(), 0, 2);
    layout->addWidget(_blurCheckBox.get(), 1, 2);
    layout->addWidget(_flameTypeCheckBox.get(), 1, 1);
    setLayout(layout);

    auto timer = new QTimer(this);
    connect(timer, &QTimer::timeout, glWidget, &GLWidget::animate);
    timer->start(50);

    this->layout()->setSizeConstraint(QLayout::SetFixedSize);
}

std::unique_ptr<QSlider> Window::createSlider(const int startPosition, const int start, const int last,
    const int step, const int tick) const
{
    auto slider = std::make_unique<QSlider>(Qt::Vertical);
    slider->setRange(start, last);
    slider->setSingleStep(step);
    slider->setTickInterval(tick);
    slider->setSliderPosition(startPosition);
    slider->setTickPosition(QSlider::TicksRight);
    return slider;
}
