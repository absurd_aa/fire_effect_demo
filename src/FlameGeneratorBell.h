#pragma once
#include "FlameGeneratorBase.h"

namespace FireEffectDemo
{
    class FlameGeneratorBell final : public FlameGeneratorBase
    {
    public:
        FlameGeneratorBell(double minDecay, double maxDecay, double initDecay, int paletteSize, size_t width, size_t height)
            : FlameGeneratorBase(minDecay, maxDecay, initDecay, paletteSize, width, height),
            _bellCenter(0.5 * width), _bellDispersion(0.1 * width)
        {
        }

        [[nodiscard]] unsigned char calcInitialIntensity(size_t) override;

    private:
        [[nodiscard]] double calcBellMultiplier(size_t x) const;

        //this parameters can be defined by application sliders
        double _bellCenter;
        double _bellDispersion;
    };

}

