#include "FlameGeneratorBell.h"

using namespace FireEffectDemo;

unsigned char FlameGeneratorBell::calcInitialIntensity(const size_t x)
{
    const auto seed = FlameGeneratorBase::calcInitialIntensity(x);
    const auto mult = calcBellMultiplier(x) * 0.5 * _width;
    return std::min(static_cast<int>(mult * seed), _paletteSize - 1);
}

double FlameGeneratorBell::calcBellMultiplier(const size_t x) const
{
    static const auto INV_SQRT_2_PI = 0.3989422804014327;
    const auto a = (x - _bellCenter) / _bellDispersion;

    return INV_SQRT_2_PI / _bellDispersion * std::exp(-0.5f * a * a);
}
