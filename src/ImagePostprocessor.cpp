#include "ImagePostprocessor.h"

#include "opencv2/imgproc.hpp"

#include "Utils.h"
using namespace FireEffectDemo;

QImage ImagePostprocessor::doBlur(QImage& im)
{
    auto src = Utils::qImageToMat(im);
    using namespace cv;
    const auto ks = calcKernelSize();
    blur(src, src, Size(ks, ks));
    return Utils::matToQImage(src);
}

void ImagePostprocessor::reset()
{
    _curStep = 0;
}

int ImagePostprocessor::calcKernelSize()
{
    const auto candidate = _curStep + (_direction > 0 ? _upStep : _downStep);
    if (candidate > _startSize && candidate < _maxSize)
    {
        _curStep = candidate;
        return static_cast<int>(candidate);
    }
    if (candidate >= _maxSize)
    {
        _curStep = _maxSize;
        _direction = -1;
        return static_cast<int>(_curStep);
    }
    _curStep = _startSize;
    _direction = 1;
    return static_cast<int>(_curStep);
}
