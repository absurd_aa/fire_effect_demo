#include "glwidget.h"

#include <QPainter>
#include <utility>

#include "ImagePostprocessor.h"

using namespace FireEffectDemo;

GLWidget::GLWidget(std::unique_ptr<FireSimulator> fireSimulator, QWidget *parent)
    : QOpenGLWidget(parent), _fireSimulator(std::move(fireSimulator)), _postProc()
{
    setFixedSize(static_cast<int>(_fireSimulator->width()), static_cast<int>(_fireSimulator->height()));
    setAutoFillBackground(false);
}

void GLWidget::animate()
{
    update();
}

void GLWidget::setHue(const int hue)
{
    _fireSimulator->setHue(hue);
}

void GLWidget::setDecay(const int decay)
{
    _fireSimulator->setDecay(decay);
}


void GLWidget::setFlame(const bool isBell)
{
    if (isBell)
        _fireSimulator->setBellFlame();
    else
        _fireSimulator->setBaseFlame();
}

void GLWidget::setBlur(const bool isBlur)
{
    _postProc.reset();
    _isBlur = isBlur;
}

void GLWidget::paintEvent(QPaintEvent *)
{
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);

    _fireSimulator->update();
    QImage image(_fireSimulator->width(), _fireSimulator->height(), QImage::Format_RGB32);
    image.fill(Qt::black);

    for (auto i = 0; i < _fireSimulator->width(); ++i)
    {
        for (auto j = _fireSimulator->height() - 1; j > static_cast<int>(_fireSimulator->minNonNullLine()); --j)
        {
            image.setPixel(i, j, _fireSimulator->getColor(i, j));
        }
    }
    painter.drawImage(QRect(0, 0, _fireSimulator->width(), _fireSimulator->height()),
        _isBlur ? _postProc.doBlur(image) : image);
    
    painter.end();
}
