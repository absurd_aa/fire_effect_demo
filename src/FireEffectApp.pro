QT          += widgets

HEADERS     = Utils.h \
              FlameGeneratorBase.h \
              FlameGeneratorBell.h \
              ImagePostprocessor.h \
              FireSimulator.h \
              glwidget.h \
              window.h
SOURCES     = Utils.cpp \
              FlameGeneratorBase.cpp \
              FlameGeneratorBell.cpp \
              ImagePostprocessor.cpp \
              FireSimulator.cpp \
              glwidget.cpp \
              main.cpp \
              window.cpp

# install
target.path = bin
INSTALLS += target

INCLUDEPATH += getenv(OpenCV_INCLUDE_DIRS)

LIBS += getenv(OpenCV_LIBS)
