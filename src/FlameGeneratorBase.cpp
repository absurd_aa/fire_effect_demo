#include "FlameGeneratorBase.h"
#include <functional>
#include "Utils.h"

using namespace FireEffectDemo;

FlameGeneratorBase::FlameGeneratorBase(const double minDecay, const double maxDecay, 
    const double initDecay, const int paletteSize,
    const size_t width, const size_t height): _paletteSize(paletteSize), _minDecay(minDecay), _maxDecay(maxDecay), _decay(initDecay),
                                        _width(width), _height(height)
{
    std::random_device rd;
    _gen = std::mt19937(rd());
    _dist = std::uniform_int_distribution<>(0, paletteSize - 1);
}


unsigned char FlameGeneratorBase::calcIntensity(const std::vector<unsigned char>& intensityTable, const size_t x, const size_t y) const
{
    return static_cast<unsigned char>(round(
        0.33 * abs((static_cast<double>(intensityTable[at(x - 1, y + 1)]) + intensityTable[at(x, y + 1)] + intensityTable[at(x + 1, y + 1)]) -
            calcDecay(x, y))
    ));
}

unsigned char FlameGeneratorBase::calcInitialIntensity(size_t)
{
    return _dist(_gen);
}

double FlameGeneratorBase::calcDecay(size_t, size_t) const
{
    return _decay;
}

void FlameGeneratorBase::setDecay(const unsigned char decayValue)
{
    _decay = static_cast<double>(decayValue) * (_maxDecay - _minDecay) / UCHAR_MAX + _minDecay;
}

size_t FlameGeneratorBase::at(const size_t x, const size_t y) const
{
    return Utils::at(x, y, _width, _height);
}
