#pragma once
#include <QOpenGLWidget>

#include "FireSimulator.h"
#include "ImagePostprocessor.h"

namespace FireEffectDemo
{
    class Helper;

    class GLWidget : public QOpenGLWidget
    {
        Q_OBJECT

    public:
        GLWidget(std::unique_ptr<FireSimulator> fireSimulator, QWidget* parent);

    public slots:
        void animate();
        void setHue(int hue);
        void setDecay(int decay);
        void setFlame(bool isBell);
        void setBlur(bool isBlur);

    protected:
        void paintEvent(QPaintEvent* event) override;

    private:
        std::unique_ptr<FireSimulator> _fireSimulator;
        ImagePostprocessor _postProc;
        bool _isBlur = false;
    };
}
