#include "Utils.h"
using namespace FireEffectDemo;

size_t Utils::at(const size_t x, const size_t y, const size_t width, const size_t height)
{
    //use % to set up behaviour for border values
    return y % height * width + (x + width) % width;
}


cv::Mat Utils::qImageToMat(QImage& img, int format)
{
    return cv::Mat(img.height(), img.width(),
                   format, img.bits(), img.bytesPerLine());
}

QImage Utils::matToQImage(const cv::Mat& mat, const QImage::Format format)
{
    return QImage(mat.data, mat.cols, mat.rows, static_cast<int>(mat.step), format);
}
